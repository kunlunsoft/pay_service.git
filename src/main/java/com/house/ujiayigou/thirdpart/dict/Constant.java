package com.house.ujiayigou.thirdpart.dict;

public class Constant {
    public static final String RESOURCE_WXPAY_PROPERTIES = "/properties/wx/wxpay.properties";
    public static final String RESOURCE_WXCP_PROPERTIES = "/properties/wx/wxcp.properties";
}
