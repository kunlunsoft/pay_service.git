package com.house.ujiayigou.thirdpart.alipay.info;

public class PayFormInfo {
    /***
     * PC扫码支付的方式，支持前置模式和跳转模式。
     前置模式是将二维码前置到商户的订单确认页的模式。需要商户在自己的页面中以iframe方式请求支付宝页面。具体分为以下几种：
     0：订单码-简约前置模式，对应iframe宽度不能小于600px，高度不能小于300px；
     1：订单码-前置模式，对应iframe宽度不能小于300px，高度不能小于600px；
     3：订单码-迷你前置模式，对应iframe宽度不能小于75px，高度不能小于75px；
     4：订单码-可定义宽度的嵌入式二维码，商户可根据需要设定二维码的大小。

     跳转模式下，用户的扫码界面是由支付宝生成的，不在商户的域名下。
     2：订单码-跳转模式
     */
    protected String qr_pay_mode;
    /***
     * 商户订单号，商户网站订单系统中唯一订单号，必填
     */
    private String out_trade_no;
    /***
     * 订单名称
     */
    private String subject;
    /***
     * 付款金额<br />
     * 单位:元,例如"3.6",或者"19.98"
     */
    private String total_amount;
    /***
     * 商品描述
     */
    private String body;
    private String timeout_express;
    /***
     * 销售产品码 必填
     */
    private String product_code = "FAST_INSTANT_TRADE_PAY";
    /***
     * 公用回传参数
     */
    private String extra_common_param;
    /***
     * wap 支付需要
     */
    private String seller_id;
    /***
     * 展示商品详情的链接<br />
     * 目前不好使
     */
    private String showUrl;

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    /***
     * 单位:元,例如"3.6",或者"19.98"
     * @return
     */
    public String getTotal_amount() {
        return total_amount;
    }

    /***
     * 单位:元,例如"3.6",或者"19.98"
     * @param total_amount
     */
    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getExtra_common_param() {
        return extra_common_param;
    }

    public void setExtra_common_param(String extra_common_param) {
        this.extra_common_param = extra_common_param;
    }

    public String getQr_pay_mode() {
        return qr_pay_mode;
    }

    public void setQr_pay_mode(String qr_pay_mode) {
        this.qr_pay_mode = qr_pay_mode;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getShowUrl() {
        return showUrl;
    }

    public void setShowUrl(String showUrl) {
        this.showUrl = showUrl;
    }
}
