package com.house.ujiayigou.thirdpart.alipay.config;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：3.3
 *日期：2012-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

 *提示：如何获取安全校验码和合作身份者ID
 *1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
 *3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”

 *安全校验码查看时，输入支付密码后，页面呈灰色的现象，怎么办？
 *解决方法：
 *1、检查浏览器配置，不让浏览器做弹框屏蔽设置
 *2、更换浏览器或电脑，重新登录查询。
 */

import com.file.hw.props.GenericReadPropsUtil;
import com.string.widget.util.ValueWidget;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class AlipayConfig {
    public static final String MY_EMAIL = "";//TODO 需要填写
    /***
     * 支付宝异步通知回调接口
     */
    public static final String notify_url = "/pay/notify";//TODO 需要填写
    public static org.slf4j.Logger logger = LoggerFactory.getLogger("pay_log");
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    // 商户appid
    public static String APPID = "";//TODO 需要填写
    // 合作身份者ID，以2088开头由16位纯数字组成的字符串
    public static String partner = "";//TODO 需要填写
    // 收款支付宝账号
    public static String seller_email = "";
    // 商户的 MD5 私钥
    public static String key = "";//TODO 需要填写
    /***
     * pkcs1格式 ,RSA1
     */
    public static String private_key = "";
    // 私钥 pkcs8格式的
    public static String RSA2_PRIVATE_KEY = "";//TODO 需要填写
    // 支付宝的RSA1 公钥
    public static String ali_public_key = "";//TODO 需要填写

    //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
    // 请求网关地址
    public static String URL = "https://openapi.alipay.com/gateway.do";
    // 调试用，创建TXT日志文件夹路径
    public static String log_path = "logs";
    // 字符编码格式 目前支持 gbk 或 utf-8
    public static String input_charset = "utf-8";
    // 返回格式
    public static String FORMAT = "json";
    // 签名方式 不需修改
    public static String sign_type = "RSA2";
    // 支付宝RSA2 公钥
    public static String ALIPAY_PUBLIC_KEY = "";//TODO 需要填写
    // 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
    public static String seller_id = partner;
    // 支付类型 ，无需修改
    public static String payment_type = "1";
    // 调用的接口名，无需修改
    public static String wap_service = "alipay.wap.create.direct.pay.by.user";
    public static String wap_return_url = "/static/html/paying.html";//TODO 需要填写
    /***
     * 超时时间
     */
    public static String TIMEOUT_EXPRESS = "12h";
    /***
     * 优先从配置文件中读取
     */
    public static String base_url = "http://i.yhskyc.com";//TODO 需要填写

    static {
        try {
            Properties properties = GenericReadPropsUtil.getProperties(true, "config/pay.properties");
            if (null == properties) {
                logger.error("没有找到配置文件:\"config/pay.properties\"");
            } else {
                String baseUrl = properties.getProperty("alipay.base_url");
                if (!ValueWidget.isNullOrEmpty(baseUrl)) {
                    base_url = baseUrl;
                }

                String timeout_express = properties.getProperty("alipay.timeout_express");
                if (!ValueWidget.isNullOrEmpty(timeout_express)) {
                    if (ValueWidget.isNumeric(timeout_express)) {
                        timeout_express = timeout_express + "h";
                    }
                    TIMEOUT_EXPRESS = timeout_express;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }

    }

}
