package com.house.ujiayigou.thirdpart.weixin;

import com.alibaba.fastjson.JSONObject;
import com.yunmasoft.service.pay.wexin.ValueParser;

import java.util.Hashtable;
import java.util.Properties;


/**
 * A lite json schema materializer implemention for building wxmp/wxmp reqs' json body.
 *
 * @since 0.7.0.0
 */
public class JSONMaterializer {
    // DEFAULT INSTANCE
    public static JSONMaterializer instance = new JSONMaterializer();
    // CONSTRUCT
    public Hashtable<String, ValueParser> parsers;

    public JSONMaterializer() {
        this.parsers = new Hashtable<String, ValueParser>();

        this.parsers.put("string", PrimitiveParsers.StringParser.instance);
        this.parsers.put("integer", PrimitiveParsers.LongParser.instance);
        this.parsers.put("number", PrimitiveParsers.DoubleParser.instance);

        return;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    // MATERIALIZE
    public JSONObject materialize(JSONObject jsonSchema, Properties p) {
        JSONObject j = new JSONObject();

        JSONObject nodes = jsonSchema.getJSONObject("properties");

        for (String k : nodes.keySet()) {
            JSONObject node = nodes.getJSONObject(k);

            String t = node.getString("type");
            Object v = null;

            if ("object".equals(t)) {
                v = this.materialize(node.getJSONObject("schema"), p);
            } else {
                v = this.parsers.get(t)
                        .parseString(
                                p.getProperty(k), null
                        );
            }

            // label put:
            j.put(k, v);
        }

        System.out.println(j);

        return (j);
    }
}
