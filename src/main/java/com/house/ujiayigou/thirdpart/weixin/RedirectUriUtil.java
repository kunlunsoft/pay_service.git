package com.house.ujiayigou.thirdpart.weixin;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by huangweii on 2017/2/15.
 */
public class RedirectUriUtil {
    public static String convertRedir(String thisUrl) {
        /*if (WapPayController.isDebug) {
            String H_CHANJET_COM = "h.chanjet.com%2fappstore%2fweixin_pay";
            thisUrl = thisUrl.replace("blog.yhskyc.com", H_CHANJET_COM);
            thisUrl = thisUrl.replace("inte-store.chanjet.com", H_CHANJET_COM);
            thisUrl = thisUrl.replace("moni-store.chanjet.com", H_CHANJET_COM);
            thisUrl = thisUrl.replace("store.chanjet.com", H_CHANJET_COM);
        }*/
        return thisUrl;
    }

    public static String getRedirectUrl(HttpServletRequest req) {
        String thisUrl = URLBuilder.encodeURIComponent(
                req.getRequestURL()
                        .append(req.getQueryString() != null ? "?" + req.getQueryString() : "")
                        .toString()
        );
            /*if (thisUrl.contains("fsnsapi-base.api")&&!thisUrl.contains("appstore")) {
                String folder = "appstore%2fweixin_pay";
                thisUrl = thisUrl.replace("%2fsnsapi-base.api", "%2f" + folder + "%2fsnsapi-base.api");
            }*/
        thisUrl = RedirectUriUtil.convertRedir(thisUrl);
        return thisUrl;
    }
}
