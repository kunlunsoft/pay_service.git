package com.yunmasoft.service.pay;

import com.common.dao.generic.GenericDao;
import com.common.util.RedisHelper;
import com.io.hw.json.HWJacksonUtils;
import com.string.widget.util.ValueWidget;
import oa.entity.AlipayNotifySuccess;
import oa.util.SpringMVCUtil;
import org.springframework.stereotype.Service;

@Service
public class AlipayNotifySuccessService {
    /*@Resource
    private AlipayNotifySuccessDao alipayNotifySuccessDao;
*/

    /***
     * 增加缓存
     * @param orderNo
     * @return
     */
    public AlipayNotifySuccess getAlipayNotifySuccess(String orderNo) {

        String redisKey = "orderNo" + orderNo + "_NotifySuccess";
        String json = RedisHelper.getInstance().getCache(redisKey);
        /*if (!useCache) {
            json = null;
        }*/
        if (ValueWidget.isNullOrEmpty(json)) {
            GenericDao genericDao = SpringMVCUtil.getBean("alipayNotifySuccessDao");
            AlipayNotifySuccess alipayNotifySuccess = (AlipayNotifySuccess) genericDao.get("out_trade_no", orderNo);
            RedisHelper.getInstance().clearCache(redisKey);
            RedisHelper.getInstance().saveExpxKeyCache(redisKey, HWJacksonUtils.getJsonP(alipayNotifySuccess), 3600 * 24 * 15);
            return alipayNotifySuccess;
        } else {
            AlipayNotifySuccess alipayNotifySuccess = (AlipayNotifySuccess) HWJacksonUtils.deSerialize(json, AlipayNotifySuccess.class);
            return alipayNotifySuccess;
        }

    }
}
