package com.yunmasoft.service.pay.wexin.exception;

/**
 * Error return while return_code is FAIL
 */
public class WxpayProtocolException extends RuntimeException {
    protected String returnMsg;

    public WxpayProtocolException(String returnMsg) {
        this.returnMsg = returnMsg;

        return;
    }

    public String getReturnMsg() {
        return (this.returnMsg);
    }

    @Override
    public String getMessage() {
        return (this.returnMsg);
    }
}
