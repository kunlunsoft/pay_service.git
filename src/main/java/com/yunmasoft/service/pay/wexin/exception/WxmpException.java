package com.yunmasoft.service.pay.wexin.exception;

public class WxmpException extends RuntimeException {
    public int errcode;
    public String errmsg;

    public WxmpException(int errcode, String errmsg) {
        super("" + errcode + ":" + errmsg);

        this.errcode = errcode;

        return;
    }
}
