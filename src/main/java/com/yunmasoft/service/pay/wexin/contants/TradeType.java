package com.yunmasoft.service.pay.wexin.contants;

public enum TradeType {
    JSAPI(),
    NATIVE(),
    APP();
}
