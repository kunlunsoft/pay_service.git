package com.yunmasoft.service.pay.wexin;


import com.yunmasoft.service.pay.wexin.contants.TradeType;

import java.util.Properties;

public class JSAPIUnifiedOrder extends UnifiedOrder {
    // COSTRUCT
    public JSAPIUnifiedOrder(Properties prop) {
        super(prop);

        super.setTradeType(TradeType.JSAPI);

        return;
    }
}
