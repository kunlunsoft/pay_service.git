package com.yunmasoft.service.pay.alipay;

import java.util.Map;

/**
 * Created by David on 15/8/31.
 */
public interface PayOperation {

    String verify(Map<String, String> params);

    String preparePostRequest(com.house.ujiayigou.thirdpart.alipay.info.PayFormInfo payFormInfo);


//    public String startPayment(String access_token, /*String orgid, */String userid, String orderid, BigDecimal totalPrice, BigDecimal couponValue);

    String payNotify(String out_trade_no, String trade_no, String trade_status, Map<String, String> params);


//    public String getOrgId(String orderId);

    Integer getPayType();
}